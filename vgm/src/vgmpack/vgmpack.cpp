// vgmpack
// Mic, 2009

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <zlib.h>
#include "dictcodec.h"
#include "nullcodec.h"
#include "rlecodec.h"

using namespace std;


void pack(char *inFileName, char *outFileName, int mode)
{
    unsigned int i, inSize, origInSize, vgmOffs, preOffs, loopOffs, newLoopOffs, progress,
		         extraDataOffs, lastPcmOffs, unzippedBytes;
	FILE *inFile, *outFile;
	gzFile inFileZ;
	unsigned char c, ch3Mode = 0;
	unsigned char *vgmData, *preProcessedData;
	vector<unsigned char> outData;
	vector<unsigned char> extraDataBlock;
	vector<unsigned char> unzippedData;
	unsigned short *dictionary;
	Codec *codec;
	bool endOfVgmData = false;
	bool vgz = false;
	static unsigned char unzipBuffer[16384];

	if ((inFileName[strlen(inFileName) - 1] == 'z') || (inFileName[strlen(inFileName) - 1] == 'Z'))
	{
		vgz = true;
	}

	if (!vgz)
	{
		fopen_s(&inFile, inFileName, "rb");
		if (inFile == NULL)
		{
			printf("Error: Failed to open %s\n", inFileName);
			exit(0);
		}
		fseek(inFile, 0, SEEK_END);
		inSize = origInSize = ftell(inFile);
		fseek(inFile, 0, SEEK_SET);
		if ((vgmData = new unsigned char[inSize]) == NULL)
		{
			puts("Error: Failed to allocate memory");
			return;
		}
		if ((preProcessedData = new unsigned char[inSize]) == NULL)
		{
			puts("Error: Failed to allocate memory");
			return;
		}
		if (fread(vgmData, 1, inSize, inFile) != inSize)
		{
			puts("Error: Failed to read VGM data");
			exit(0);
		}
		fclose(inFile);
	}
	else
	{
		puts("Unzipping file");
		inFileZ = gzopen(inFileName, "rb");
		if (inFileZ == NULL)
		{
			printf("Error: Failed to gzopen %s\n", inFileName);
			exit(0);
		}
		while (true)
		{
			unzippedBytes = gzread(inFileZ, unzipBuffer, 16384);
			if (unzippedBytes > 0)
			{
				for (i = 0; i < unzippedBytes; i++)
				{
					unzippedData.push_back(unzipBuffer[i]);
				}
			}
			else
			{
				break;
			}
		}
		gzclose(inFileZ);
		inSize = origInSize = (unsigned int)unzippedData.size();
		if ((vgmData = new unsigned char[inSize]) == NULL)
		{
			printf("Error: Failed to allocate memory (wanted %d bytes)\n", inSize);
			return;
		}
		for (i = 0; i < inSize; i++)
		{
			vgmData[i] = unzippedData[i];
		}
		unzippedData.clear();	
		preProcessedData = new unsigned char[inSize];
	}

	if ((vgmData[8] != 0x50) || (vgmData[9] != 0x01) ||
		(vgmData[10] != 0x00) || (vgmData[11] != 0x00))
	{
		puts("Only VGM version 1.50 supported, output will not be compressed");
		fopen_s(&outFile, outFileName, "wb");
		if (outFile == NULL)
		{
			delete [] vgmData;
			delete [] preProcessedData;
			printf("Error: unable to open %s\n", outFileName);
		}
		else
		{
			fwrite(vgmData, 1, inSize, outFile);
			fclose(outFile);
			delete [] vgmData;
			delete [] preProcessedData;
		}
		return;
	}
	vgmData[8] = 0x51;	// To identify the VGM as compressed

	vgmOffs = vgmData[0x34] +
		      (vgmData[0x35] << 8) +
			  (vgmData[0x36] << 16) +
			  (vgmData[0x37] << 24) + 0x34;
	extraDataOffs = vgmOffs;

	loopOffs = vgmData[0x1C] +
		       (vgmData[0x1D] << 8) +
			   (vgmData[0x1E] << 16) +
			   (vgmData[0x1F] << 24) + 0x1C;
	newLoopOffs = loopOffs;

	printf("Packing %s:", inFileName);

	for (i = 0; i < vgmOffs; i++)
	{
		outData.push_back(vgmData[i]);
	}

	if (mode == 0)
	{
		codec = new NullCodec(&outData);
	}
	else if (mode == 1)
	{
		codec = new RleCodec(&outData);
	}
	else
	{
		codec = new DictCodec(&outData);
		if (mode == 3)
		{
			codec->altCodec = new RleCodec(&outData);
			mode = 2;
		}
	}

	for (i = 0; i < vgmOffs; i++)
	{
		preProcessedData[i] = vgmData[i];
	}
	preOffs = vgmOffs;
	lastPcmOffs = 0xFFFFFFFF;

	// Run a pre-processing stage to remove redundant commands
	while (!endOfVgmData)
	{
		if (vgmOffs == loopOffs)
		{
			newLoopOffs = preOffs;
			//printf("[1] loop %x -> %x\n", loopOffs, newLoopOffs);
		}

		c = vgmData[vgmOffs++];

		switch (c)
		{
			case 0x52:
				c = vgmData[vgmOffs++];
				if (c == 0x27)
				{
					c = vgmData[vgmOffs++];
					if ((c >> 6) != ch3Mode)
					{
						ch3Mode = c >> 6;
						preProcessedData[preOffs++] = 0x52;
						preProcessedData[preOffs++] = 0x27;
						preProcessedData[preOffs++] = c;
					}
				}
				else if ((c == 0x25) || (c == 0x26))
				{
					vgmOffs++;
				}
				else
				{
					preProcessedData[preOffs++] = 0x52;
					preProcessedData[preOffs++] = c;
					preProcessedData[preOffs++] = vgmData[vgmOffs++];
				}
				break;

			case 0x51:
			case 0x53:
			case 0x54:
			case 0x61:
				preProcessedData[preOffs++] = c;
				preProcessedData[preOffs++] = vgmData[vgmOffs++];
				preProcessedData[preOffs++] = vgmData[vgmOffs++];
				break;

			case 0x66:
				preProcessedData[preOffs++] = c;
				endOfVgmData = true;
				break;

			case 0x67:
				if (vgmData[vgmOffs] == 0x66)
				{
					unsigned int dataSize = vgmData[vgmOffs+2];
					dataSize += vgmData[vgmOffs+3] << 8;
					dataSize += vgmData[vgmOffs+4] << 16;
					dataSize += vgmData[vgmOffs+5] << 24;
					preProcessedData[preOffs++] = c;
					for (i = 0; i < dataSize + 6; i++)
					{
						preProcessedData[preOffs++] = vgmData[vgmOffs++];
					}
				}
				else
				{
					printf("Illegal command: 0x67 0x%02x\n", vgmData[vgmOffs]);
					delete [] vgmData;
					delete [] preProcessedData;
					delete codec;
					exit(0);
				}
				break;

			case 0xE0:
				i = vgmData[vgmOffs++];
				i += vgmData[vgmOffs++] << 8;
				i += vgmData[vgmOffs++] << 16;
				i += vgmData[vgmOffs++] << 24;
				if ((mode == 0) || (i != 0))
				{
					preProcessedData[preOffs++] = c;
					preProcessedData[preOffs++] = i & 0xFF;
					preProcessedData[preOffs++] = (i >> 8) & 0xFF;
					preProcessedData[preOffs++] = (i >> 16) & 0xFF;
					preProcessedData[preOffs++] = (i >> 24) & 0xFF;
					lastPcmOffs = i;
				}
				else if (mode != 0)
				{
					preProcessedData[preOffs++] = 0x4C;
				}
				break;

			case 0x80: case 0x81: case 0x82: case 0x83: case 0x84: case 0x85: case 0x86: case 0x87:
			case 0x88: case 0x89: case 0x8A: case 0x8B: case 0x8C: case 0x8D: case 0x8E: case 0x8F:
				c &= 0x0F;
				if ((vgmData[vgmOffs] & 0xF0) == 0x70)
				{
					while ((vgmData[vgmOffs] & 0xF0) == 0x70)
					{
						if ((c + (vgmData[vgmOffs] & 0x0F)) < 0x10)
						{
							c += vgmData[vgmOffs] & 0x0F;
							vgmOffs++;
						}
						else
						{
							break;
						}
					}
					preProcessedData[preOffs++] = 0x80 | c;
				}
				else if (preOffs > 0)
				{
					if ((preProcessedData[preOffs - 1] & 0xF0) == 0x70)
					{
						if ((c + (preProcessedData[preOffs - 1] & 0x0F)) < 0x10)
						{
							c += preProcessedData[preOffs - 1] & 0x0F;
						}
						preProcessedData[preOffs - 1] = 0x80 | c;
					}
					else
					{
						preProcessedData[preOffs++] = 0x80 | c;
					}
				}
				else
				{
					preProcessedData[preOffs++] = 0x80 | c;
				}
				break;

			default:
				preProcessedData[preOffs++] = c;
				break;

		}
	}
	while (vgmOffs < inSize)
	{
		preProcessedData[preOffs++] = vgmData[vgmOffs++];
	}

	loopOffs = newLoopOffs;
	vgmOffs = extraDataOffs;
	endOfVgmData = false;
	delete [] vgmData;
	vgmData = preProcessedData;
	inSize = preOffs;

	progress = 0;

	// Now do the encoding stage
	while (!endOfVgmData)
	{
		if (((vgmOffs * 10) / inSize) > progress)
		{
			progress++;
			printf("%c", 177);
		}

		if (vgmOffs == loopOffs)
		{
			codec->flush();
			newLoopOffs = (unsigned int)outData.size();
			//printf("[2] loop %x -> %x\n", loopOffs, newLoopOffs);
		}

		c = vgmData[vgmOffs++];

		codec->write(c);

		switch (c)
		{
			case 0x4F:
			case 0x50:
				codec->passThrough(vgmData[vgmOffs++]);
				break;

			case 0x51:
			case 0x52:
			case 0x53:
			case 0x54:
			case 0x61:
				codec->passThrough(vgmData[vgmOffs++]);
				codec->passThrough(vgmData[vgmOffs++]);
				break;

			case 0x66:
				endOfVgmData = true;
				break;

			case 0x67:
				if (vgmData[vgmOffs] == 0x66)
				{
					unsigned int dataSize = vgmData[vgmOffs+2];
					dataSize += vgmData[vgmOffs+3] << 8;
					dataSize += vgmData[vgmOffs+4] << 16;
					dataSize += vgmData[vgmOffs+5] << 24;
					for (i = 0; i < dataSize + 6; i++)
					{
						codec->passThrough(vgmData[vgmOffs++]);
					}
				}
				else
				{
					printf("Illegal command: 0x67 0x%02x\n", vgmData[vgmOffs]);
					delete [] vgmData;
					delete codec;
					exit(0);
				}
				break;

			case 0xE0:
				codec->passThrough(vgmData[vgmOffs++]);
				codec->passThrough(vgmData[vgmOffs++]);
				codec->passThrough(vgmData[vgmOffs++]);
				codec->passThrough(vgmData[vgmOffs++]);
				break;
		}
	}

	if (mode == 2)
	{
		extraDataBlock.push_back(0x67);
		extraDataBlock.push_back(0x66);
		extraDataBlock.push_back(0x01);
		extraDataBlock.push_back(DICT_SIZE & 0xFF);
		extraDataBlock.push_back(DICT_SIZE >> 8);
		extraDataBlock.push_back(0x00);
		extraDataBlock.push_back(0x00);
		dictionary = (unsigned short *)(codec->get(0));
		for (i = 0; i < DICT_SIZE; i++)
		{
			extraDataBlock.push_back((unsigned char)dictionary[i]);
		}
	}

	delete codec;

	// EOF offset
	i = (unsigned int)(outData.size() + extraDataBlock.size() - 4);
	outData[4] = i & 0xFF;
	outData[5] = (i >> 8) & 0xFF;
	outData[6] = (i >> 16) & 0xFF;
	outData[7] = (i >> 24) & 0xFF;

	// Read rest of data, if any (GD3)
	while (vgmOffs < inSize)
	{
		outData.push_back(vgmData[vgmOffs++]);
	}

	// GD3 offset
	i = outData[0x14] +
		(outData[0x15] << 8) +
		(outData[0x16] << 16) +
		(outData[0x17] << 24);
	if (i)
	{
		i -= (origInSize - (outData.size() + extraDataBlock.size()));
		outData[0x14] = i & 0xFF;
		outData[0x15] = (i >> 8) & 0xFF;
		outData[0x16] = (i >> 16) & 0xFF;
		outData[0x17] = (i >> 24) & 0xFF;
	}

	// Loop offset
	if (loopOffs > 0x1C)
	{
		newLoopOffs += (unsigned int)extraDataBlock.size();
		newLoopOffs -= 0x1C;
		outData[0x1C] = newLoopOffs & 0xFF;
		outData[0x1D] = (newLoopOffs >> 8) & 0xFF;
		outData[0x1E] = (newLoopOffs >> 16) & 0xFF;
		outData[0x1F] = (newLoopOffs >> 24) & 0xFF;
	}

	while (progress < 10)
	{
		printf("%c", 177);
		progress++;
	}

	printf("\nInput size: %d bytes, output size: %d bytes\n", origInSize, outData.size() + extraDataBlock.size());

	fopen_s(&outFile, outFileName, "wb");
	if (outFile == NULL)
	{
		delete [] vgmData;
		printf("Error: unable to open %s\n", outFileName);
	}
	else
	{
		for (i = 0; i < extraDataOffs; i++)
		{
			fputc(outData[i], outFile);
		}
		for (unsigned int j = 0; j < extraDataBlock.size(); j++)
		{
			fputc(extraDataBlock[j], outFile);
		}
		for (; i < outData.size(); i++)
		{
			fputc(outData[i], outFile);
		}
		fclose(outFile);
	}

	delete [] vgmData;
	outData.clear();
}


int main(int argc, char *argv[])
{
	int i, mode = 1;
	char *inFn = NULL, *outFn = NULL;

	puts("VGM Packer by Mic, 2009");

	for (i = 1; i < argc; i++)
	{
		if (argv[i][0] == '-')
		{
			if ((strcmp(argv[i], "-h")==0) ||
				(strcmp(argv[i], "-help")==0) ||
				(strcmp(argv[i], "-?")==0))
			{
				printf("Usage: vgmpack [options] <input> <output>\n");
				printf("Options:\n\t-0\tOnly pre-process\n\t-1\tRun-length compression\n");
				printf("\t-2\tDictionary compression\n\t-3\tRun-length + dictionary compression\n");
				return 0;
			}
			else if (strcmp(argv[i], "-0")==0)
			{
				mode = 0;
			}
			else if (strcmp(argv[i], "-1")==0)
			{
				mode = 1;
			}
			else if (strcmp(argv[i], "-2")==0)
			{
				mode = 2;
			}
			else if (strcmp(argv[i], "-3")==0)
			{
				mode = 3;
			}
		}
		else if (inFn == NULL)
		{
			inFn = argv[i];
			if (inFn[0] == '\"')
			{
				inFn[strlen(inFn) - 1] = '\0';
				inFn++;
			}
		}
		else if (outFn == NULL)
		{
			outFn = argv[i];
			if (outFn[0] == '\"')
			{
				outFn[strlen(outFn) - 1] = '\0';
				outFn++;
			}
		}
	}

	if ((inFn == NULL) || (outFn == NULL))
	{
		printf("Usage: vgmpack [options] <input> <output>\n");
		printf("Use -h for a description of the options\n");
        return 0;
    }

	pack(inFn, outFn, mode);

	return 0;
}


