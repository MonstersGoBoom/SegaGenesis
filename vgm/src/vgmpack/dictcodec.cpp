// Dictionary codec implementation for vgmpack
// Mic, 2009

#include "dictcodec.h"


void DictCodec::passThrough(unsigned char c)
{
	outData->push_back(c);
}


void DictCodec::flush()
{
	if (stringToMatch.size() > 3)
	{
		if (altCodec)
		{
			altCodec->flush();
		}
		if (stringToMatch.size() <= 16)
		{
			outData->push_back(0xC0 | (unsigned char)(stringToMatch.size() - 1));
			outData->push_back(candidates[0]);
		}
		else
		{
			outData->push_back(0x4D);
			outData->push_back((unsigned char)(stringToMatch.size() - 1));
			outData->push_back(candidates[0]);
		}
	}
	else
	{
		if (altCodec)
		{
			for (unsigned int i = 0; i < stringToMatch.size(); i++)
			{
				altCodec->write(stringToMatch[i]);
			}
			altCodec->flush();
		}
		else
		{
			for (unsigned int i = 0; i < stringToMatch.size(); i++)
			{
				outData->push_back(stringToMatch[i]);
			}
		}
	}
	candidates.clear();
	stringToMatch.clear();
}


void DictCodec::writeHelper(unsigned char c)
{
	unsigned int idx, n;
	
	if (candidates.size())
	{
		n = 0;
		stringToMatch.push_back(c);
		while (n < candidates.size())
		{ 
			idx = (unsigned int)((candidates[n] + stringToMatch.size() - 1) & (DICT_SIZE - 1));
			if (dictionary[idx] == (unsigned short)c)
			{
				if (stringToMatch.size() == DICT_SIZE)
				{
					if (altCodec)
					{
						altCodec->flush();
					}
					outData->push_back(0x4D);
					outData->push_back(0xFF);
					outData->push_back(candidates[n]);
					candidates.clear();
					stringToMatch.clear();
					return;
				}
				else
				{
					n++;
				}
			}
			else if (dictionary[idx] == DICT_SLOT_EMPTY)
			{
				if ((lastStoredInDict == (unsigned short)c) && (run >= DICT_THRESHOLD))
				{
					// Avoid filling up the dictionary with just the same character
					this->flush();
				}
				else
				{
					dictionary[idx] = (unsigned short)c;
					numCharsInDict++;
				}
				if (lastStoredInDict == (unsigned short)c)
				{
					run++;
				}
				else
				{
					run = 1;
				}
				lastStoredInDict = (unsigned short)c;
				return;
			}
			else
			{
				if (candidates.size() == 1)
				{
					if ((DICT_SIZE - numCharsInDict) > stringToMatch.size())
					{
						// Add the string at the end of the dictionary
						candidates[0] = numCharsInDict;
						for (unsigned int i = 0; i < stringToMatch.size(); i++)
						{
							dictionary[numCharsInDict++] = stringToMatch[i];
							if (lastStoredInDict == (unsigned short)stringToMatch[i])
							{
								run++;
							}
							else
							{
								lastStoredInDict = (unsigned short)stringToMatch[i];
								run = 1;
							}
						}
					}
					else
					{
						this->flush();
					}
				}
				else
				{
					candidates.erase(candidates.begin() + n);
				}

			}
		}
	}
	else
	{
		if (stringToMatch.size())
		{
			for (unsigned int i = 0; i < stringToMatch.size(); i++)
			{
				if (altCodec)
				{
					altCodec->write(stringToMatch[i]);
				}
				else
				{
					outData->push_back(stringToMatch[i]);
				}
			}
			stringToMatch.clear();
			if (altCodec)
			{
				altCodec->write(c);
			}
			else
			{
				outData->push_back(c);
			}
		}
		else
		{
			if ((lastStoredInDict == (unsigned short)c) && (run >= DICT_THRESHOLD))
			{
				// Avoid filling up the dictionary with just the same character
				if (altCodec)
				{
					altCodec->write(c);
				}
				else
				{
					outData->push_back(c);
				}
				run++;
			}
			else
			{
				//for (int i = 0; i < DICT_SIZE; i++)
				{
					//if (dictionary[i] == DICT_SLOT_EMPTY)
					if (numCharsInDict < DICT_SIZE)
					{
						dictionary[numCharsInDict++] = (unsigned short)c;
						lastStoredInDict = (unsigned short)c;
						run = 1;
						//break;
					}
				}
				for (int i = 0; i < DICT_SIZE; i++)
				{
					if (dictionary[i] == (unsigned short)c)
					{
						candidates.push_back(i);
					}
				}
				stringToMatch.push_back(c);
			}
		}
	}
}


void DictCodec::write(unsigned char c)
{
	if ((c >= 0x80) && (c <= 0x8F))
	{
		writeHelper(c);
	}
	else
	{
		this->flush();
		if (altCodec)
		{
			altCodec->flush();
		}
		passThrough(c);
	}
}
