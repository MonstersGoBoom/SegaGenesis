// VGM Player ROM Builder
// Mic, 2009

#pragma once


namespace rombuilder {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			this->AllowDrop = true;
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::ListBox^  listBox1;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  textBox1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->listBox1 = (gcnew System::Windows::Forms::ListBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			//
			// button1
			//
			this->button1->Location = System::Drawing::Point(12, 392);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Build";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			//
			// listBox1
			//
			this->listBox1->AllowDrop = true;
			this->listBox1->FormattingEnabled = true;
			this->listBox1->Location = System::Drawing::Point(12, 25);
			this->listBox1->Name = L"listBox1";
			this->listBox1->Size = System::Drawing::Size(533, 186);
			this->listBox1->TabIndex = 2;
			this->listBox1->DragDrop += gcnew System::Windows::Forms::DragEventHandler(this, &Form1::listBox1_DragDrop);
			this->listBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::listBox1_SelectedIndexChanged);
			this->listBox1->DragOver += gcnew System::Windows::Forms::DragEventHandler(this, &Form1::listBox1_DragOver);
			//
			// label1
			//
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(9, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(106, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"Files (drag and drop):";
			//
			// label2
			//
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 247);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(40, 13);
			this->label2->TabIndex = 5;
			this->label2->Text = L"Status:";
			//
			// textBox1
			//
			this->textBox1->Location = System::Drawing::Point(15, 263);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->textBox1->Size = System::Drawing::Size(529, 117);
			this->textBox1->TabIndex = 6;
			//
			// Form1
			//
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(557, 427);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->listBox1);
			this->Controls->Add(this->button1);
			this->Name = L"Form1";
			this->Text = L"VGMPlay ROM Builder";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	/*private: System::Void .ctor() {
				 this->InitializeComponent();
			 }*/
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
			 }
	/*private: System::Void .ctor() {
				 this->InitializeComponent();
			 }
	private: System::Void .ctor() {
				 this->InitializeComponent();
			 }
	private: System::Void .ctor() {
				 this->InitializeComponent();
			 }*/
	private: System::Void listBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {

			 }

	private: System::Void listBox1_DragOver(System::Object^  sender, System::Windows::Forms::DragEventArgs^  e)
	{
		if( e->Data->GetDataPresent(DataFormats::FileDrop, false) == true )
		{
			 e->Effect = DragDropEffects::Copy;
		}
		else
		{
			 e->Effect = DragDropEffects::None;
		}
	}

	private: System::Void listBox1_DragDrop(System::Object^  sender, System::Windows::Forms::DragEventArgs^  e)
	{
		// Ensure that the list item index is contained in the data.
		if( e->Data->GetDataPresent(DataFormats::FileDrop, false) == true )
		{
			array<System::String ^>^ files = (array<System::String ^>^)(e->Data->GetData( DataFormats::FileDrop ));
			// Perform drag-and-drop, depending upon the effect.
			if ( e->Effect == DragDropEffects::Copy || e->Effect == DragDropEffects::Move )
			{
				for (int i = 0; i < files->Length; i++)
					listBox1->Items->Add( files[i] );
			}
		}
	}

	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e)
	{
	    int c = listBox1->Items->Count;
		System::IO::FileInfo^ fi1 = gcnew System::IO::FileInfo("player.bin");
		System::IO::FileStream^ fs = System::IO::File::Create("VGM_PLAY.BIN");
		System::IO::BinaryWriter^ vgmplay = gcnew System::IO::BinaryWriter(fs);
		long vgmOffs = (long)fi1->Length + 4 + c*4;
		System::Collections::ArrayList packedFiles;
		textBox1->Clear();

		System::IO::FileStream^ fs3 = System::IO::File::Open("player.bin", System::IO::FileMode::Open);
		System::IO::BinaryReader^ playerFile = gcnew System::IO::BinaryReader(fs3);
		vgmplay->Write(playerFile->ReadBytes((int)fi1->Length));
		playerFile->Close();
		fs3->Close();
		vgmplay->Write(System::Net::IPAddress::HostToNetworkOrder(c));

		for (int i = 0; i < c; i++)
		{
			System::String^ args = "-3 ";
			System::String^ infile = listBox1->Items[0]->ToString();
			if (infile->EndsWith(".vgm") || infile->EndsWith(".VGM") ||
				infile->EndsWith(".vgz") || infile->EndsWith(".VGZ"))
			{
				if (infile->IndexOf(" ") < 0)
				{
					args = args->Concat(args, infile);
				}
				else
				{
					args = args->Concat(args, "\"", infile, "\"");
				}
				args = args->Concat(args, " ");
				System::String^ outfile = infile->Concat(infile, ".packed");
				if (outfile->IndexOf(" ") < 0)
				{
					args = args->Concat(args, outfile);
				}
				else
				{
					args = args->Concat(args, "\"", outfile, "\"");
				}
				System::Diagnostics::ProcessStartInfo^ psi =
					gcnew System::Diagnostics::ProcessStartInfo("vgmpack.exe");
				psi->RedirectStandardOutput = true;
				psi->Arguments = args;
				psi->WindowStyle = System::Diagnostics::ProcessWindowStyle::Hidden;
				psi->UseShellExecute = false;
				System::Diagnostics::Process^ vgmpack;
				vgmpack = System::Diagnostics::Process::Start(psi);
				System::IO::StreamReader^ myOutput = vgmpack->StandardOutput;
				vgmpack->WaitForExit();
				if (vgmpack->HasExited)
				{
					textBox1->AppendText(myOutput->ReadToEnd());
				}
				System::IO::FileInfo^ fi2 = gcnew System::IO::FileInfo(outfile);
				packedFiles.Add(outfile);
				vgmplay->Write(System::Net::IPAddress::HostToNetworkOrder(vgmOffs));
				vgmOffs += (long)fi2->Length;
				if (vgmOffs & 3) vgmOffs = (vgmOffs & 0xFFFFFFFC) + 4;


			}
			listBox1->Items->RemoveAt(0);
	    }
		vgmOffs = 0;
		System::Array^ packedFiles2 = packedFiles.ToArray();
		for (int i = 0; i < packedFiles.Count; i++)
		{
			System::IO::FileStream^ fs2 = System::IO::File::Open((System::String^)(packedFiles2->GetValue(i)), System::IO::FileMode::Open);
			System::IO::BinaryReader^ packedFile = gcnew System::IO::BinaryReader(fs2);
			System::IO::FileInfo^ fi3 = gcnew System::IO::FileInfo((System::String^)(packedFiles2->GetValue(i)));
			vgmOffs += (long)fi3->Length;
			vgmplay->Write(packedFile->ReadBytes((int)fi3->Length));
			while (vgmOffs & 3)
			{
				vgmplay->Write((unsigned char)0);
				vgmOffs++;
			}
			packedFile->Close();
			fs2->Close();
		}
		vgmplay->Close();
		fs->Close();
    }
};
}

