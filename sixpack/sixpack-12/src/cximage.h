/*
  CxImage wrapper interface
  
  /Mic 2009/2010
*/

#ifndef __CXIMAGE_H__
#define __CXIMAGE_H__

//#include <windows.h>
#include <stdio.h>
#include "dib.h"
#include <dlfcn.h>

// Image types. Used with Load() and Save().
enum
{
	CXI_FORMAT_UNKNOWN = 0,
	CXI_FORMAT_BMP = 1,
	CXI_FORMAT_GIF = 2,
	CXI_FORMAT_JPG = 3,
	CXI_FORMAT_PNG = 4,
	CXI_FORMAT_ICO = 5,
	CXI_FORMAT_TGA = 6,
	CXI_FORMAT_PCX = 7
};

enum
{
	CXI_GOOD_RESIZE = 0,
	CXI_FAST_RESIZE = 1
};


enum
{
	CXI_FROM_FILE = 1,
	CXI_FROM_MEMORY = 0
};

typedef int* (*PFNCXILOAD)(char*,int,int,int);
typedef int (*PFNCXISAVE)(int*,char*,int,int,int);
typedef void (*PFNCXIFREE)(int*);
typedef int (*PFNCXIGETXY)(int*);
typedef unsigned char* (*PFNCXIGETBITS)(int*);
typedef void (*PFNCXIRESIZE)(int*,int,int,int);



class CxImage
{
public:
    CxImage()
    {   
        if (hLib == NULL)
        {
	    hLib = dlopen("libcximage_wrap.so", RTLD_LAZY);

            //hLib = (HMODULE)LoadLibraryA("cximage.dll");
            if (hLib == NULL)
            {
                printf("error loading libcximage_wrap.so\n");
                return;
       	    }

            cxi_load    = (PFNCXILOAD)dlsym(hLib, "CXI_LoadImage");
            cxi_save    = (PFNCXISAVE)dlsym(hLib, "CXI_SaveImage");
            cxi_free    = (PFNCXIFREE)dlsym(hLib, "CXI_FreeImage");
            cxi_getbits = (PFNCXIGETBITS)dlsym(hLib, "CXI_GetBits");
            cxi_getw    = (PFNCXIGETXY)dlsym(hLib, "CXI_GetWidth");
            cxi_geth    = (PFNCXIGETXY)dlsym(hLib, "CXI_GetHeight");
            cxi_getbpp  = (PFNCXIGETXY)dlsym(hLib, "CXI_GetBpp");
            cxi_getpalsize  = (PFNCXIGETXY)dlsym(hLib, "CXI_GetPaletteSize");
	    cxi_getpal  = (PFNCXIGETBITS)dlsym(hLib, "CXI_GetPalette");
	    cxi_resize  = (PFNCXIRESIZE)dlsym(hLib, "CXI_Resize");
		if (cxi_load == NULL)
		{
		printf("failed to load one or more functions from libcximage_wrap.so\n");
		return;
		}
        }
                	
        img = NULL;
    }
    
	~CxImage()
	{
		free();
	}

    bool load(char *fileName, int imgType);
    void free();
    
    int getWidth()
    {
	printf("Calling cxi_getw\n");	
        return cxi_getw(img);
    }
    
    int getHeight()
    {
        return cxi_geth(img);
    }
    
	void resize(int width, int height, int mode)
	{
		cxi_resize(img, width, height, mode);
	}

	unsigned char *getPalette();

    DIB *getDIB();
            
private:
	static void* hLib;
	static PFNCXILOAD cxi_load;
	static PFNCXISAVE cxi_save;
	static PFNCXIFREE cxi_free;
	static PFNCXIGETXY cxi_getw,cxi_geth,cxi_getbpp,cxi_getpalsize;
	static PFNCXIGETBITS cxi_getbits,cxi_getpal;
	static PFNCXIRESIZE cxi_resize;

	int *img;
          
};


#endif
