m68k-elf-as -m68000 --register-prefix-optional -o m68k_crt0.o m68k_crt0.s
m68k-elf-ld -Tmd.ld --oformat binary -o m68k_crt0.bin m68k_crt0.o
m68k-elf-as -m68000 --register-prefix-optional -o m68k_crt1.o m68k_crt1.s
m68k-elf-ld -Tmd.ld --oformat binary -o m68k_crt1.bin m68k_crt1.o
sh-elf-as -o sh2_crt0.o sh2_crt0.s

..\..\bin\sixpack.exe -image -pack -v -target 32x -format l8 -q 256 -width 320 -height 224 -sizefilter 1 -o maruko.lzs -preview preview.bmp ..\images\maruko.png

sh-elf-as -o lzss_decode.o ..\..\decoder\32x\lzss_decode.s
sh-elf-as -o image.o image_lzss.s
sh-elf-gcc -c -O2 -o main.o main_lzss.c
sh-elf-ld -T 32x.ld -e _start --oformat binary -o test_lzss.32x sh2_crt0.o main.o image.o lzss_decode.o
del image.o
del lzss_decode.o
del main.o
del maruko.lzs
del maruko.pal


..\..\bin\sixpack.exe -image -pack -v -target 32x -format rl8 -q 256 -width 320 -height 224 -sizefilter 1 -o maruko.rle -preview preview.bmp ..\images\maruko.png

sh-elf-as -o image.o image_rle.s
sh-elf-gcc -c -O2 -o main.o main_rle.c
sh-elf-ld -T 32x.ld -e _start --oformat binary -o test_rle.32x sh2_crt0.o main.o image.o 
del image.o
del main.o
del maruko.rle
del maruko.ltb
del maruko.pal


..\..\bin\sixpack.exe -image -pack -v -target 32x -codec aplib -format l8 -q 256 -width 320 -height 224 -sizefilter 1 -o maruko.apx -preview preview.bmp ..\images\maruko.png

sh-elf-as -o aplib_decrunch.o ..\..\decoder\32x\aplib_decrunch.s
sh-elf-as -o image.o image_apx.s
sh-elf-gcc -c -O2 -o main.o main_aplib.c
sh-elf-ld -T 32x.ld -e _start --oformat binary -o test_aplib.32x sh2_crt0.o main.o image.o aplib_decrunch.o


..\..\bin\sixpack.exe -image -v -target 32x -format l8 -q 256 -width 320 -height 96 -sizefilter 1 -o maruko.pat -preview preview.bmp ..\images\parodius2.png
REM packfire -t maruko.pat maruko.pfi

sh-elf-as -o unpackfire_tiny.o ..\..\decoder\32x\unpackfire_tiny.s
sh-elf-as -o image.o image_pfi.s
sh-elf-gcc -c -O2 -o main.o main_packfire.c
sh-elf-ld -T 32x.ld -e _start -Map foo.map --oformat binary -o test_packfire.32x sh2_crt0.o main.o image.o unpackfire_tiny.o
del image.o
del unpackfire_tiny.o
del maruko.pat
del aplib_decrunch.o
del main.o
del maruko.apx
del maruko.pal



del m68k_crt0.bin
del m68k_crt1.bin
del m68k_crt0.o
del m68k_crt1.o
del sh2_crt0.o

 