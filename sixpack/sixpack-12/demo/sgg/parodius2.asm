; Created by Sixpack on 04/22/09 13:53:56

.DEFINE LZSS_DICTIONARY_SIZE 2048
.DEFINE LZSS_THRESHOLD 2
.DEFINE LZSS_LEN_BITS 5
.DEFINE LZSS_LEN_MASK $1f
.DEFINE LZSS_MAX_LEN 34
.DEFINE LZSS_PLANES_USED 4
.DEFINE LZSS_FORMAT_PLANES 4

parodius2_pattern:
.incbin "parodius2.lzs"
parodius2_pattern_end:

parodius2_pattern_decode:
ld ix,parodius2_pattern
ld iy,parodius2_pattern_end-parodius2_pattern
call lzss_decode_vram
