; Test program for aPLib pattern decoding
; SMS version
; /Mic, 2010


.memorymap
	defaultslot 0

	slotsize $4000
	slot 0 0
.endme

.rombanksize $4000
.rombanks 1


.bank 0 
.orga $0000

.include "smsvdp.inc"

 
di
im 	1
ld 	sp,$dff0			; Set stack pointer
jp 	main


; IRQ handler
.orga $0038
	ei
	reti


; NMI handler	
.orga $0066
	retn


main:
	; Reset scrolling
	VDP_SETREG 8,0
	VDP_SETREG 9,0

	; Set nametable base address to $3800
	VDP_SETREG 2,14 
	
	; Decode pattern data to RAM
	ld	hl,patterns
	ld	de,$C000
	call 	aplib_depack

	; Copy the decoded patterns from RAM to VRAM
	ld	a,0
	out	($BF),a
	or	$40
	out	($BF),a
	ld	hl,$C000
	ld	de,161*32
-:
	ld	a,(hl)
	inc	hl
	out	($BE),a
	dec	de
	ld	a,e
	or	d
	jr	nz,-
	
	; Set palette
	VDP_SETCRAMADR $00
	ld	hl,palette
	ld	c,VDP_DATA
	ld	b,palette_end-palette
	otir
	
	; Clear the nametable
	VDP_SETVRAMADR $3800 
	ld	de,$600		; Clear 24 rows * 40 bytes = $600 bytes
	ld 	a,0
clear_nt:
	out	(VDP_DATA),a
	dec	e
	jr	nz,clear_nt
	dec 	d
	jr	nz,clear_nt


	; Display the image
	ld	de,$3800+4*$40+8*2
	ld	hl,nametable
	ld	c,VDP_DATA
	ld	b,16
-:
	push	bc
	
	; Set VRAM address
	ld	a,e
	out	(VDP_CTRL),a
	ld	a,d
	or	$40
	out	(VDP_CTRL),a	
	
	; Copy 16 name table entries
	ld	b,32
	otir

	; Increase the VRAM address by 64 bytes (move to next row)
	ld	a,e
	add	a,$40
	ld	e,a
	ld	a,d
	adc	a,0
	ld	d,a
	
	pop	bc
	dec	b
	jr	nz,-


	; Enable the screen (mode 4 / 192 lines), vblank irqs
	VDP_SETREG 0,$C4
	VDP_SETREG 1,$60
	VDP_SETREG 3,$FF
	VDP_SETREG 4,$07
	VDP_SETREG 7,$00

	ei				; Enable interrupts

forever:
	halt				; Sit and wait for interrupts
	jp 	forever		



; Include the aPLib decoder
.include "..\..\decoder\sms_gg\aplib_z80.asm"


patterns:
	.incbin "ufolog.apx"
palette:
	.incbin "ufolog.pal"
palette_end:

nametable:
	.incbin "ufolog.nam"
	

; Padding
.orga $3ffe
.dw 0
