; Test program for aPLib pattern decoding
; TurboGrafx version
; /Mic, 2010

.memorymap
        defaultslot 0
        slotsize $2000
        slot 0 $0000
        slot 1 $2000
        slot 2 $4000
        slot 3 $6000
        slot 4 $8000
        slot 5 $A000
        slot 6 $C000
        slot 7 $E000
.endme

.rombankmap
        bankstotal 8
        banksize $2000
        banks 8
.endro


.DEFINE VDC_CTRL			$0000
.DEFINE VDC_DATA_L			$0002
.DEFINE VDC_DATA_H			$0003

.DEFINE VCE_INDEX_L			$0402
.DEFINE VCE_INDEX_H			$0403
.DEFINE VCE_DATA_L			$0404
.DEFINE VCE_DATA_H			$0405


.macro INCW
	inc	<\1
	bne	+
	inc	<\1+1
	+:
.endm


; Zeropage variables
.enum $2000
VRAMPTR		ds 2
NAMPTR		ds 2
.ende


.org $0000
.dw 0


; Interrupt vectors
.org $1FF6

 .dw vdc_irq                    
 .dw vdc_irq                   
 .dw timer_irq               
 .dw vdc_irq               
 .dw start     


 
.bank 0 slot 7
.org $0000
.section "text"

.include "..\..\decoder\tgx\aplib_decrunch.asm"

start:
    sei                               
    csh  							; switch the CPU to high speed mode                             
    cld                               
    ldx    	#$FF                       
    txs
    
    ; Setup memory bank registers
    lda		#$FF
    tam		#$01
    lda		#$F8
    tam		#$02
    lda		#$01
    tam		#$04
    lda		#$02
    tam		#$08
    lda		#$03
    tam		#$10
    lda		#$04
    tam		#$20
    lda		#$05
    tam		#$40
    lda		#$00
    tam		#$80
    
    stz    	$2000                	; clear all the RAM
    tii    	$2000,$2001,$1FFF


	; Setup VDC registers	
	clx
-:
	lda.w	vdc_reg_values,x
	cmp		#$FF
	beq		+
	sta.w	VDC_CTRL
	inx
	lda.w	vdc_reg_values,x
	sta.w	VDC_DATA_L
	inx
	lda.w	vdc_reg_values,x
	sta.w	VDC_DATA_H
	inx
	bra		-
+:

	; Clear the screen
	st0		#0
	st1		#0
	st2		#0					; VRAM address = 0
	st0		#2
	ldy		#4
-:
	clx
--:
	st1		#0
	st2		#0
	dex
	bne		--
	dey
	bne		-
	

	; Decompress the tiles to RAM at $2400
	lda		#<tiles
	sta		<APLIB_SRC
	lda		#>tiles
	sta		<APLIB_SRC+1
	lda		#$00
	sta		<APLIB_DEST
	lda		#$24
	sta		<APLIB_DEST+1
	jsr		aplib_decrunch

	; I get weird results in mednafen if I don't reset the VDC control register before every major VRAM transfer
	st0		#5
	st1		#0
	st2		#0
	
	; Unchain the decompressed tile data and copy it to VRAM
	lda		#$00
	sta		<APLIB_SRC
	lda		#$24
	sta		<APLIB_SRC+1
	lda		#$00
	sta		<APLIB_DEST
	lda		#$08
	sta		<APLIB_DEST+1
	jsr		aplib_unchain_tia
	

	; Load palette (6 colors)
	stz.w	VCE_INDEX_L
	stz.w	VCE_INDEX_H
	tia		palette,VCE_DATA_L,6*2


	st0		#5
	st1		#0
	st2		#0
	; Load the nametable (the BAT in VRAM is 32x32 tiles, while the image we're loading is 16x16)
	lda		#<(7*32 + 8)
	sta		<VRAMPTR
	lda		#>(7*32 + 8)
	sta		<VRAMPTR+1
	lda		#<nametable
	sta		<NAMPTR
	lda		#>nametable
	sta		<NAMPTR+1
	ldy		#16
-:
	; Set VRAM address
	st0		#0
	lda		<VRAMPTR
	sta.w	VDC_DATA_L
	lda		<VRAMPTR+1
	sta.w	VDC_DATA_H
	st0		#2
	ldx		#16
--:
	lda		(<NAMPTR)
	clc
	adc		#64						; the tiles loaded at $800 in VRAM, so we need to offset the tile indices by ($800 / $20) = $40
	sta.w	VDC_DATA_L
	INCW	NAMPTR
	lda		(<NAMPTR)
	sta.w	VDC_DATA_H
	INCW	NAMPTR
	dex
	bne		--
	lda		<VRAMPTR
	clc
	adc		#32
	sta		<VRAMPTR
	lda		<VRAMPTR+1
	adc		#0
	sta		<VRAMPTR+1
	dey
	bne		-

	; Enable the background
	st0		#5
	st1		#$80
	st2		#0
	
	cli


forever:
	bra		forever
 
 
 
timer_irq:
vdc_irq:
	rti
	
	

vdc_reg_values:
.db $00
.dw $0400
.db $05
.db $0000
.db $07
.dw $0000
.db $08
.dw $0000
.db $09
.dw $0000
.db $0A
.dw $0202
.db $0B
.dw $031F
.db $0C
.dw $0F02
.db $0D
.dw $00EF
.db $0E
.dw $0003
.db $FF


tiles:
.incbin "ufolog.apx"

nametable:
.incbin "ufolog.nam"

palette:
.incbin "ufolog.pal"

 .ends
 
