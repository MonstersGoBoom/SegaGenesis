..\..\bin\sixpack.exe -pack -codec aplib -image -opt -chain -target tgx -v -width 128 -height 128 -sizefilter 1 -planes 4 -o ufolog.apx -preview preview.bmp ..\images\ufolog.png

REM example of decrunching to RAM
wla-huc6280 -vo test_aplib.asm test.o
wlalink -b test.link test_aplib.pce

REM example of decrunching to VRAM
wla-huc6280 -vo test_aplib_vram.asm test.o
wlalink -b test.link test_aplib_vram.pce

REM c:\pcedev\mednafen-0.8.D-win32\mednafen.exe test_aplib.pce

del test.o
del ufolog.apx
del ufolog.pal
del ufolog.nam




