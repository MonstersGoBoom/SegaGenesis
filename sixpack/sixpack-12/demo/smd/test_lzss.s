# Test program for LZSS pattern decoding
# SMD version
# /Mic, 2009


.text
.globl main


.include "genvdp.inc"


main:
	# Initialize VDP 
	jsr 		init_gfx

	# Wait until the next vblank
	jsr		wait_vsync
	
	# Decode pattern data
  	move.l #(end_patterns-patterns),d0
  	move.l #patterns,a2
  	move.l #0x0000,a3
  	jsr lzss_decode_vram

	# Create an empty tile (with only color 0)
	# Used for clearing map B
	move.l 		#GFXCNTL,a2
	VRAM_ADDR 	d0,0x7D00	/* Tile number 1000 */
	move.l 		d0,(a2)
	move.l 		#GFXDATA,a3
	move.l		#15,d4
_create_empty_tile:
	move.w 		#0,(a3)
	dbra		d4,_create_empty_tile

	# Load palette 
	move.l		#0x0000,a3
	move.l 		#palette,a4
	move.l		#16,d4
	jsr		load_colors
	
	jsr		wait_vsync

	# Display the image on map A
	move.l 		#GFXCNTL,a2
	VRAM_ADDR 	d0,0xE000
	move.l 		d0,(a2)
	move.l 		#GFXDATA,a3
	move.l		#nametable,a4
	move.l		#(32*28)-1,d4
_copy_nametable_data:
	move.w 		(a4)+,(a3)
	dbra		d4,_copy_nametable_data

	# Clear map B with the empty tile created earlier
	move.l 		#GFXCNTL,a2
	VRAM_ADDR 	d0,0xC000
	move.l 		d0,(a2)
	move.l 		#GFXDATA,a3
	move.l		#(32*28)-1,d4
_clear_map_b:
	move.w 		#1000,(a3)
	dbra		d4,_clear_map_b

forever:
	jsr		wait_vsync
	bra 		forever
	


#################################################
#                                               #
#         Initialize VDP registers              #
#                                               #
#################################################

init_gfx:
	move.l 		#GFXCNTL,a3
	write_vdp_reg 	0,(VDP0_E_HBI + VDP0_E_DISPLAY + VDP0_PLTT_FULL)
	write_vdp_reg 	1,(VDP1_E_VBI + VDP1_E_DISPLAY + VDP1_E_DMA + VDP1_PAL + VDP1_RESERVED)
	write_vdp_reg 	2,(0xe000 >> 10)	/* Screen map a adress */
	write_vdp_reg 	3,(0xe000 >> 10)	/* Window address */
	write_vdp_reg 	4,(0xc000 >> 13)	/* Screen map b address */
	write_vdp_reg 	5,(0xfc00 >>  9)	/* Sprite address */
	write_vdp_reg 	6,0	
	write_vdp_reg	7,0			/* Border color */
	write_vdp_reg	8,1			/* Unused (?) */
	write_vdp_reg	9,1			/* Unused (?) */
	write_vdp_reg	10,1			/* Lines per hblank interrupt */
	write_vdp_reg	11,4			/* 2-cell vertical scrolling */
	write_vdp_reg	12,(VDP12_SCREEN_V224 + VDP12_SCREEN_H256 + VDP12_PROGRESSIVE)
	write_vdp_reg	13,(0x6000 >> 10)	/* Horizontal scroll address */
	write_vdp_reg	15,2
	write_vdp_reg	16,(VDP16_MAP_V32 + VDP16_MAP_H32)
	write_vdp_reg	17,0
	write_vdp_reg	18,0xff
	rts



#################################################
#                                               #
#        Load tile data from ROM                #
#                                               #
# Parameters:                                   #
#  a3: VRAM base                                # 
#  a4: pattern address                          #
#  d4: number of tiles to load                  #
#                                               #
#################################################

load_tiles:
	move.l 		#GFXCNTL,a2
	VRAM_ADDR_var 	d0,a3
	move.l 		d0,(a2)
	lsl		#3,d4
	subq.l		#1,d4	
	move.l 		#GFXDATA,a3
_copy_tile_data:
	move.l 		(a4)+,(a3)
	dbra 		d4,_copy_tile_data

	rts



#################################################
#                                               #
#        Load color data from ROM               #
#                                               #
# Parameters:                                   #
#  a3: CRAM base                                # 
#  a4: color list address                       #
#  d4: number of colors to load                 #
#                                               #
#################################################

load_colors:
	move.l 		#GFXCNTL,a2
	CRAM_ADDR_var 	d0,a3
	move.l 		d0,(a2)

	move.l 		#GFXDATA,a3
	subq.l		#1,d4
_copy_color_data:
	move.w		(a4)+,(a3)
	dbra		d4,_copy_color_data

	rts

	
#################################################
#                                               #
#       Wait for next VBlank interrupt          #
#                                               #
#################################################

wait_vsync:
	movea.l		#vtimer,a0
	move.l		(a0),a1
_wait_change:
	cmp.l		(a0),a1
	beq		_wait_change
	rts



#################################################
#                                               #
#                 ROM DATA                      #
#                                               #
#################################################

patterns:
	.incbin "maruko.lzs"
end_patterns:
	.align 2
palette:
	.incbin "maruko.pal"
	.align 2
nametable:
	.incbin "maruko.nam"




#################################################
#                                               #
#                 RAM DATA                      #
#                                               #
#################################################

.bss
.globl htimer
.globl vtimer
.globl rand_num
htimer:		.long 0
vtimer:		.long 0
rand_num:	.long 0

.end






