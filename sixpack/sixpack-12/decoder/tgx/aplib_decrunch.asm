; #######################################################################################################################################

; apLib decruncher for the PC-Engine
; /Mic, 2010
;
; Assembles with wla-dx
;
; Example (decrunching packed_data into RAM at $2200):
;
;	lda		#<packed_data
;	sta		<APLIB_SRC
;	lda		#>packed_data
;	sta		<APLIB_SRC+1
;	lda		#$00
;	sta		<APLIB_DEST
;	lda		#$22
;	sta		<APLIB_DEST+1
;	jsr		aplib_decrunch
;

; #######################################################################################################################################

; Zeropage variables
.EQU APLIB_LWM 		$2010
.EQU APLIB_BITS 	$2012
.EQU APLIB_BITCOUNT $2013
.EQU APLIB_OFFS 	$2014
.EQU APLIB_OFFS2 	$2016
.EQU APLIB_GAMMA 	$2018
.EQU APLIB_SRC 		$201A
.EQU APLIB_DEST 	$201C
.EQU APLIB_SRC2 	$201E
.EQU APLIB_OLDDEST	$2020
.EQU APLIB_RAMCODE	$2028
.EQU APLIB_NUMBYTES	$2018
.EQU APLIB_COUNT	$2014
.EQU APLIB_PLANAR	$2038

; #######################################################################################################################################

; Increase a 16-bit zeropage variable
.macro apl_inc16_zp 
	inc	<\1
	bne	+
	inc	<\1+1
 	+:
 .endm

; Decrease a 16-bit zeropage variable
.macro apl_dec16_zp 
	lda	<\1
	bne	+
	dec	<\1+1
 	+:
 	dec	<\1
 .endm
 

; Add an 8-bit zeropage variable to a 16-bit zeropage variable
.macro apl_add16_8_zp 
	lda 	<\1
	clc
 	adc 	<\2
 	sta 	<\1
 	rol	<\1+1
 .endm 


; Subtract one zeropage variable from another
.macro apl_sub16_zp 
	lda 	<\1
 	sec
 	sbc 	<\2
 	sta 	<\1
 	lda 	<\1+1
 	sbc 	<\2+1
 	sta 	<\1+1
 .endm


.macro apl_mov16_zp 
	lda 	<\2
	sta 	<\1
	lda 	<\2+1
	sta 	<\1+1
 .endm
 
; #######################################################################################################################################
 
	
; In:
; APLIB_SRC = source address
; APLIB_DEST = dest address
;
; Out:
; APLIB_NUMBYTES = uncompressed size in bytes
;
aplib_decrunch:
	tii	_ad_ramcode_start,APLIB_RAMCODE,_ad_ramcode_end-_ad_ramcode_start
	
	apl_mov16_zp APLIB_OLDDEST,APLIB_DEST

	stz 	<APLIB_OFFS+1
	stz	<APLIB_LWM+1
	lda	#1
	sta	<APLIB_BITCOUNT
_ad_copy_byte:
	lda	(<APLIB_SRC)
	sta	(<APLIB_DEST)
	apl_inc16_zp APLIB_SRC
	apl_inc16_zp APLIB_DEST
_ad_next_sequence_init:
	stz	<APLIB_LWM
_ad_next_sequence:
	jsr	_ad_get_bit
	bcc	_ad_copy_byte		; if bit sequence is %0..., then copy next byte
	jsr	_ad_get_bit
	bcc 	_ad_code_pair		; if bit sequence is %10..., then is a code pair
	jsr	_ad_get_bit
	stz	<APLIB_OFFS
	stz	<APLIB_OFFS+1
	bcs	_ad_skip_jmp
	jmp	_ad_short_match		; if bit sequence is %110..., then is a short match
_ad_skip_jmp:
	; The sequence is %111..., the next 4 bits are the offset (0-15)
	jsr	_ad_get_bit
	rol	<APLIB_OFFS
	jsr	_ad_get_bit
	rol	<APLIB_OFFS
	jsr	_ad_get_bit
	rol	<APLIB_OFFS
	jsr	_ad_get_bit
	rol	<APLIB_OFFS
	lda	<APLIB_OFFS
	beq	_ad_write_byte		; if offset == 0, then write 0x00
	
	; If offset != 0, then write the byte at destination - offset
	apl_mov16_zp APLIB_SRC2,APLIB_DEST
	apl_sub16_zp APLIB_SRC2,APLIB_OFFS
	lda	(<APLIB_SRC2)
_ad_write_byte:
	sta	(<APLIB_DEST)
	apl_inc16_zp APLIB_DEST
	jmp	_ad_next_sequence_init

	; Code pair %10...
_ad_code_pair:
	jsr	_ad_decode_gamma
	apl_dec16_zp APLIB_GAMMA
	apl_dec16_zp APLIB_GAMMA
	lda	<APLIB_GAMMA
	ora	<APLIB_GAMMA+1
	bne	_ad_normal_code_pair
	lda	APLIB_LWM
	bne	_ad_normal_code_pair
	jsr	_ad_decode_gamma
	apl_mov16_zp APLIB_OFFS,APLIB_OFFS2
	jmp	_ad_copy_code_pair
_ad_normal_code_pair:
	apl_add16_8_zp APLIB_GAMMA,APLIB_LWM
	apl_dec16_zp APLIB_GAMMA
	lda	<APLIB_GAMMA
	sta	<APLIB_OFFS+1
	lda	(<APLIB_SRC)
	sta	<APLIB_OFFS
	apl_inc16_zp APLIB_SRC
	jsr	_ad_decode_gamma
	lda	<APLIB_OFFS+1
	cmp	#$7D					; OFFS >= 32000 ?
	bcc	_ad_compare_1280
	apl_inc16_zp APLIB_GAMMA
_ad_compare_1280:
	cmp	#$05					; OFFS >= 1280 ?
	bcc	_ad_compare_128
	apl_inc16_zp APLIB_GAMMA
	jmp	_ad_continue_short_match
_ad_compare_128:
	cmp	#1
	bcs	_ad_continue_short_match
	lda	<APLIB_OFFS
	cmp	#128					; OFFS < 128 ?
	bcs	_ad_continue_short_match
	apl_inc16_zp APLIB_GAMMA
	apl_inc16_zp APLIB_GAMMA
	jmp	_ad_continue_short_match
	
; get_bit: Get bits from the crunched data and insert the most significant bit in the carry flag.
_ad_get_bit:
	dec	<APLIB_BITCOUNT
	bne	_ad_still_bits_left
	lda	#8
	sta	<APLIB_BITCOUNT
	lda	(<APLIB_SRC)
	sta	<APLIB_BITS
	apl_inc16_zp APLIB_SRC
_ad_still_bits_left:
	asl	<APLIB_BITS
	rts

; decode_gamma: Decode values from the crunched data using gamma code
_ad_decode_gamma:
	lda	#1
	sta	<APLIB_GAMMA
	stz	<APLIB_GAMMA+1
_ad_get_more_gamma:
	jsr	_ad_get_bit
	rol	<APLIB_GAMMA
	rol	<APLIB_GAMMA+1
	jsr	_ad_get_bit
	bcs	_ad_get_more_gamma
	rts

; Short match %110...
_ad_short_match:  
	lda	#1
	sta	<APLIB_GAMMA
	stz	<APLIB_GAMMA+1
	lda	(<APLIB_SRC)	; Get offset (offset is 7 bits + 1 bit to mark if copy 2 or 3 bytes) 
	apl_inc16_zp APLIB_SRC
	lsr	a
	beq	_ad_end_decrunch
	rol	<APLIB_GAMMA
	sta	<APLIB_OFFS
	stz	<APLIB_OFFS+1
_ad_continue_short_match:
	apl_mov16_zp APLIB_OFFS2,APLIB_OFFS
_ad_copy_code_pair:
	;apl_mov16_zp APLIB_SRC2,APLIB_DEST
	;apl_sub16_zp APLIB_SRC2,APLIB_OFFS

	;apl_mov16_zp APLIB_RAMCODE+1,APLIB_SRC2
	apl_mov16_zp APLIB_RAMCODE+1,APLIB_DEST
	apl_sub16_zp APLIB_RAMCODE+1,APLIB_OFFS	
	apl_mov16_zp APLIB_RAMCODE+3,APLIB_DEST
	apl_mov16_zp APLIB_RAMCODE+5,APLIB_GAMMA
	jmp	APLIB_RAMCODE
_ad_after_copy:
	lda	<APLIB_DEST
	clc
	adc	<APLIB_GAMMA
	sta	<APLIB_DEST
	lda	<APLIB_DEST+1
	adc	<APLIB_GAMMA+1
	sta	<APLIB_DEST+1
	lda	#1
	sta	<APLIB_LWM
	jmp	_ad_next_sequence
	
_ad_end_decrunch:
	apl_mov16_zp APLIB_NUMBYTES,APLIB_DEST
	apl_sub16_zp APLIB_NUMBYTES,APLIB_OLDDEST
	rts

_ad_ramcode_start:
	tii $0000,$0000,$0000
	jmp _ad_after_copy
_ad_ramcode_end:


; aplib_unchain_tia
;
; Converts pattern data from linear to planar format and sends the result to VRAM.
;
; In:
; APLIB_SRC = source address
; APLIB_DEST = VRAM byte address
; APLIB_NUMBYTES = number of bytes to copy
;
aplib_unchain_tia:
	lsr	<APLIB_DEST+1
	ror	<APLIB_DEST
	st0	#0
	lda	<APLIB_DEST
	sta.w	$0002
	lda	<APLIB_DEST+1
	sta.w	$0003
	st0	#2
_aut_loop_tiles:
	lda	#8
	sta	<APLIB_COUNT
	clx
_aut_tile:
	ldy	#4
-:
	lda	(<APLIB_SRC)
	lsr	a
	rol	<APLIB_PLANAR,x
	lsr	a
	rol	<APLIB_PLANAR+1,x
	lsr	a
	rol	<APLIB_PLANAR+16,x
	lsr	a
	rol	<APLIB_PLANAR+17,x
	lsr	a
	rol	<APLIB_PLANAR,x
	lsr	a
	rol	<APLIB_PLANAR+1,x
	lsr	a
	rol	<APLIB_PLANAR+16,x
	lsr	a
	rol	<APLIB_PLANAR+17,x
	apl_inc16_zp APLIB_SRC		
	dey
	bne	-
	inx
	inx
	dec	<APLIB_COUNT
	bne	_aut_tile
	tia	APLIB_PLANAR,$0002,32
	lda	<APLIB_NUMBYTES
	sec
	sbc	#32
	sta	<APLIB_NUMBYTES
	lda	<APLIB_NUMBYTES+1
	sbc	#0
	sta	<APLIB_NUMBYTES+1
	ora	<APLIB_NUMBYTES
	bne	_aut_loop_tiles
	rts



	