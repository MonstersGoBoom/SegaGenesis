; #######################################################################################################################################

; apLib decruncher for the PC-Engine (VRAM version)
; /Mic, 2010
;
; Assembles with wla-dx
;
; Example (decrunching packed_data into VRAM at $0800):
;
;	lda		#<packed_data
;	sta		<APLIB_SRC
;	lda		#>packed_data
;	sta		<APLIB_SRC+1
;	lda		#$00
;	sta		<APLIB_DEST
;	lda		#$08
;	sta		<APLIB_DEST+1
;	jsr		aplib_decrunch_vram
;

; #######################################################################################################################################

; Zeropage variables
.EQU APLIB_LWM 		$2010
.EQU APLIB_BITS 	$2012
.EQU APLIB_BITCOUNT $2013
.EQU APLIB_OFFS 	$2014
.EQU APLIB_OFFS2 	$2016
.EQU APLIB_GAMMA 	$2018
.EQU APLIB_SRC 		$201A
.EQU APLIB_DEST 	$201C
.EQU APLIB_SRC2 	$201E
.EQU APLIB_OLDDEST	$2020
.EQU APLIB_RAMCODE	$2028
.EQU APLIB_NUMBYTES	$2018
.EQU APLIB_COUNT	$2014
.EQU APLIB_COUNT2	$2016
.EQU APLIB_PLANAR	$2038

; #######################################################################################################################################

; Increase a 16-bit zeropage variable
.macro apl_inc16_zp 
	inc	<\1
	bne	+
	inc	<\1+1
 	+:
 .endm

; Decrease a 16-bit zeropage variable
.macro apl_dec16_zp 
	lda	<\1
	bne	+
	dec	<\1+1
 	+:
 	dec	<\1
 .endm
 

; Add an 8-bit zeropage variable to a 16-bit zeropage variable
.macro apl_add16_8_zp 
	lda 	<\1
	clc
 	adc 	<\2
 	sta 	<\1
 	rol	<\1+1
 .endm 


; Subtract one zeropage variable from another
.macro apl_sub16_zp 
	lda 	<\1
 	sec
 	sbc 	<\2
 	sta 	<\1
 	lda 	<\1+1
 	sbc 	<\2+1
 	sta 	<\1+1
 .endm


.macro apl_mov16_zp 
	lda 	<\2
	sta 	<\1
	lda 	<\2+1
	sta 	<\1+1
 .endm
 
; #######################################################################################################################################
 
	
; In:
; APLIB_SRC = source address
; APLIB_DEST = VRAM byte address
;
aplib_decrunch_vram:
	apl_mov16_zp APLIB_OLDDEST,APLIB_DEST

	stz	<APLIB_OFFS+1
	stz	<APLIB_LWM+1
	lda	#1
	sta	<APLIB_BITCOUNT
	
	st0 #0
	lda <APLIB_DEST+1
	lsr	a
	pha
	lda <APLIB_DEST
	ror	a
	sta.w $0002
	pla
	sta.w $0003
	st0 #2
	
	clx
	cly
_adv_copy_byte:
	lda	(<APLIB_SRC)
	sta.w $0002,y
	tya
	eor #1
	tay
	apl_inc16_zp APLIB_SRC
	apl_inc16_zp APLIB_DEST
_adv_next_sequence_init:
	stz	<APLIB_LWM
_adv_next_sequence:
	jsr	_adv_get_bit
	bcc	_adv_copy_byte		; if bit sequence is %0..., then copy next byte
	jsr	_adv_get_bit
	bcc _adv_code_pair		; if bit sequence is %10..., then is a code pair
	jsr	_adv_get_bit
	stz	<APLIB_OFFS
	stz	<APLIB_OFFS+1
	bcs	_adv_skip_jmp
	jmp	_adv_short_match		; if bit sequence is %110..., then is a short match
_adv_skip_jmp:
	; The sequence is %111..., the next 4 bits are the offset (0-15)
	jsr	_adv_get_bit
	rol	<APLIB_OFFS
	jsr	_adv_get_bit
	rol	<APLIB_OFFS
	jsr	_adv_get_bit
	rol	<APLIB_OFFS
	jsr	_adv_get_bit
	rol	<APLIB_OFFS
	lda	<APLIB_OFFS
	beq	_adv_write_byte		; if offset == 0, then write 0x00
	
	; If offset != 0, then write the byte at destination - offset
	apl_mov16_zp APLIB_SRC2,APLIB_DEST
	apl_sub16_zp APLIB_SRC2,APLIB_OFFS
	tya
	beq +
	jsr	_adv_flush_write_latch
+:	
	ldx <APLIB_SRC2
	lsr <APLIB_SRC2+1
	ror <APLIB_SRC2
	st0 #1
	lda <APLIB_SRC2
	sta.w $0002
	lda <APLIB_SRC2+1
	sta.w $0003
	st0	#2
	txa
	and #1
	tax
	lda.w $0002,x 
_adv_write_byte:
	apl_inc16_zp APLIB_DEST
	sta.w $0002,y
	tya
	eor #1
	tay
	jmp	_adv_next_sequence_init

	; Code pair %10...
_adv_code_pair:
	jsr	_adv_decode_gamma
	apl_dec16_zp APLIB_GAMMA
	apl_dec16_zp APLIB_GAMMA
	lda	<APLIB_GAMMA
	ora	<APLIB_GAMMA+1
	bne	_adv_normal_code_pair
	lda	APLIB_LWM
	bne	_adv_normal_code_pair
	jsr	_adv_decode_gamma
	apl_mov16_zp APLIB_OFFS,APLIB_OFFS2
	jmp	_adv_copy_code_pair
_adv_normal_code_pair:
	apl_add16_8_zp APLIB_GAMMA,APLIB_LWM
	apl_dec16_zp APLIB_GAMMA
	lda	<APLIB_GAMMA
	sta	<APLIB_OFFS+1
	lda	(<APLIB_SRC)
	sta	<APLIB_OFFS
	apl_inc16_zp APLIB_SRC
	jsr	_adv_decode_gamma
	lda	<APLIB_OFFS+1
	cmp	#$7D					; OFFS >= 32000 ?
	bcc	_adv_compare_1280
	apl_inc16_zp APLIB_GAMMA
_adv_compare_1280:
	cmp	#$05					; OFFS >= 1280 ?
	bcc	_adv_compare_128
	apl_inc16_zp APLIB_GAMMA
	jmp	_adv_continue_short_match
_adv_compare_128:
	cmp	#1
	bcs	_adv_continue_short_match
	lda	<APLIB_OFFS
	cmp	#128					; OFFS < 128 ?
	bcs	_adv_continue_short_match
	apl_inc16_zp APLIB_GAMMA
	apl_inc16_zp APLIB_GAMMA
	jmp	_adv_continue_short_match
	
; get_bit: Get bits from the crunched data and insert the most significant bit in the carry flag.
_adv_get_bit:
	dec	<APLIB_BITCOUNT
	bne	_adv_still_bits_left
	lda	#8
	sta	<APLIB_BITCOUNT
	lda	(<APLIB_SRC)
	sta	<APLIB_BITS
	apl_inc16_zp APLIB_SRC
_adv_still_bits_left:
	asl	<APLIB_BITS
	rts

; decode_gamma: Decode values from the crunched data using gamma code
_adv_decode_gamma:
	lda	#1
	sta	<APLIB_GAMMA
	stz	<APLIB_GAMMA+1
_adv_get_more_gamma:
	jsr	_adv_get_bit
	rol	<APLIB_GAMMA
	rol	<APLIB_GAMMA+1
	jsr	_adv_get_bit
	bcs	_adv_get_more_gamma
	rts

; Short match %110...
_adv_short_match:  
	lda	#1
	sta	<APLIB_GAMMA
	stz	<APLIB_GAMMA+1
	lda	(<APLIB_SRC)	; Get offset (offset is 7 bits + 1 bit to mark if copy 2 or 3 bytes) 
	apl_inc16_zp APLIB_SRC
	lsr	a
	;bne +
	;jmp _adv_end_decrunch
;+:
	beq	_adv_end_decrunch
	rol	<APLIB_GAMMA
	sta	<APLIB_OFFS
	stz	<APLIB_OFFS+1
_adv_continue_short_match:
	apl_mov16_zp APLIB_OFFS2,APLIB_OFFS
_adv_copy_code_pair:
	apl_mov16_zp APLIB_SRC2,APLIB_DEST
	apl_sub16_zp APLIB_SRC2,APLIB_OFFS

	lda <APLIB_SRC2
	and	#1
	tax

_adv_loop_do_copy:
	; This ensures that reading from address X return the right values before X+1 has been written (when X is even)
	tya
	beq +
	jsr	_adv_flush_write_latch
+:
	; The VDC reloads the VRAM read buffer with 16 bits of data after reading the read buffer MSB.
	; This causes problems when doing a copy from address X to X+1 because read buffer will be
	; reloaded before the data has been written. Hence this otherwise wasteful practice of setting MARR
	; for every iteration; it's just to ensure that the correct data is read.
	st0 #1						; VDC register 1 (MARR)
	lda <APLIB_SRC2+1
	lsr	a
	pha
	lda	<APLIB_SRC2
	ror a
	sta.w $0002
	pla
	sta.w $0003
	st0	#2						; VDC register 2 (VRR/VWR)
	apl_inc16_zp APLIB_SRC2

 	lda.w $0002,x
	sta.w $0002,y
 
	apl_inc16_zp APLIB_DEST

	txa
	eor #1
	tax
	tya
	eor #1
	tay
	apl_dec16_zp APLIB_GAMMA
	lda		<APLIB_GAMMA
	ora		<APLIB_GAMMA+1
	bne		_adv_loop_do_copy

	lda	#1
	sta	<APLIB_LWM
	jmp	_adv_next_sequence
	
_adv_end_decrunch:
	apl_mov16_zp APLIB_NUMBYTES,APLIB_DEST
	apl_sub16_zp APLIB_NUMBYTES,APLIB_OLDDEST
	rts



_adv_flush_write_latch:
	st2 #0						; write anything to the MSB just to force the write latch to be flushed
	; Reset the destination address
	st0 #0
	lda <APLIB_DEST+1
	lsr	a
	pha
	lda <APLIB_DEST
	ror	a
	sta.w $0002
	pla
	sta.w $0003
	st0 #2
	rts


; aplib_unchain
;
; Converts pattern data in VRAM from linear to planar format 
;
; In:
; APLIB_SRC = VRAM byte address
; APLIB_DEST = VRAM byte address
; APLIB_NUMBYTES = number of bytes to copy
;
aplib_unchain:
	lsr	<APLIB_DEST+1
	ror	<APLIB_DEST
	st0	#0
	lda	<APLIB_DEST
	sta.w	$0002
	lda	<APLIB_DEST+1
	sta.w	$0003

	lsr	<APLIB_SRC+1
	ror	<APLIB_SRC
	st0	#1
	lda	<APLIB_SRC
	sta.w	$0002
	lda	<APLIB_SRC+1
	sta.w	$0003

	st0	#2
-:
	lda	#8
	sta	<APLIB_COUNT
	clx
	cly
--:
	lda	#4
	sta	<APLIB_COUNT2
---:
	lda.w	$0002,y
	lsr	a
	rol	<APLIB_PLANAR,x
	lsr	a
	rol	<APLIB_PLANAR+1,x
	lsr	a
	rol	<APLIB_PLANAR+16,x
	lsr	a
	rol	<APLIB_PLANAR+17,x
	lsr	a
	rol	<APLIB_PLANAR,x
	lsr	a
	rol	<APLIB_PLANAR+1,x
	lsr	a
	rol	<APLIB_PLANAR+16,x
	lsr	a
	rol	<APLIB_PLANAR+17,x
	tya
	eor	#1
	tay
	dec	<APLIB_COUNT2
	bne	---
	inx
	inx
	dec	<APLIB_COUNT
	bne	--
	tia	APLIB_PLANAR,$0002,32
	lda	<APLIB_NUMBYTES
	sec
	sbc	#32
	sta	<APLIB_NUMBYTES
	lda	<APLIB_NUMBYTES+1
	sbc	#0
	sta	<APLIB_NUMBYTES+1
	ora	<APLIB_NUMBYTES
	bne	-
	rts
	


	