Simple C tool to convert data from Cosmigo Promotion NG to Sega Asm files 

single file. should compile on anything with GCC 

export map as GBA format. with CEL for the tiles . 555 for the palette 
toasm.exe -o*PREFIX* "*FILE*.map" "*FILE*.cel" "*FILE*.pal"

export map as GBA format. or a single cell wide BMP file 
toasm.exe -o*PREFIX* "*FILE*.map" "*FILE*.bmp" "*FILE*.pal"

*PREFIX* is the subname of the tile 
for example
-oBLOBIT 
the output will be 

BLOBIT_map.asm.

BLOBIT_cel.asm.

BLOBIT_pal.asm.


