//
// toasm.exe -oflip "flip\Untitled 1#layer~Layer 1#map001.map" "flip\Untitled 1#tiles.cel" "flip\Untitled 1#tiles.pal"
//
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
int	priority = false;

#pragma pack(1)

//	1024 tiles max ? 
// COSMIGO GBA
// 	151413		11 				10			9876543210
//	unused 		vflip			hflip		tile index
typedef struct
{
	uint16_t tile:10;
	uint16_t hflip:1;
	uint16_t vflip:1;
	uint16_t pad:3;
} COSMIGO_TILE;

//	SEGA
//	15				14-13		12						11							109876543210
//	Priority	Palette	VerticalFlip	HorizontalFlip	Tile Index
//	2048 tiles max ?

typedef struct
{
	uint16_t tile:11;
	uint16_t hflip:1;
	uint16_t vflip:1;
	uint16_t clut:2;
	uint16_t priority:1;
} SEGA_TILE;

//	555 palette
typedef struct
{
	uint16_t r:5;
	uint16_t g:5;
	uint16_t b:5;
	uint16_t a:1;
} RGB5551_PAL;

typedef struct
{
	uint16_t r:4;
	uint16_t g:4;
	uint16_t b:4;
	uint16_t a:4;
} SEGA_PAL;

//	bmp header file
typedef struct
{
	uint16_t magic;
	uint32_t fileSize;
	uint32_t reserved0;
	uint32_t bitmapDataOffset;
	uint32_t bitmapHeaderSize;
	uint32_t width;
	uint32_t height;
	uint16_t planes;
	uint16_t bitsPerPixel;
	uint32_t compression;
	uint32_t bitmapDataSize;
	uint32_t hRes;
	uint32_t vRes;
	uint32_t colors;
	uint32_t importantColors;
} BMPHeader_t;

char *base_name = "base";

//	get the extension of the filename
//	".map" for example
const char *get_filename_ext(const char *filename)
{
	const char *dot = strrchr(filename, '.');
	if(!dot || dot == filename) return "";
	return dot;
}

//	check for - in the string
//	return NULL or string
const char *get_argument(const char *input)
{
	const char *dot = strrchr(input, '-');
	if(!dot) return "";
	return dot;
}

//	load a file 
//	allocate buffer, and return void * of data . size = length
void *disk_load(const char *fname,uint32_t *size)
{
FILE 		*fp;
int 		len = 0;
void *buffer=NULL;

	fp = fopen(fname,"rb");
	if (fp==NULL)
	{
		fprintf(stderr,"file %s not found\n");
		exit(0);
	}

	fseek(fp,0,SEEK_END);
	len = ftell(fp);
	fseek(fp,0,SEEK_SET);

	buffer = malloc(len+1);
	memset(buffer,0,len+1);
	int result = fread(buffer,len,1,fp);
	fclose(fp);
	if (size!=NULL)
		*size=len;
	return buffer;
}

//	make a new file with the basename and .asm
FILE *newfile(const char *type)
{
char b[_MAX_PATH];
	sprintf(b,"%s%s.bin",base_name,type);
	return fopen(b,"wb");
}

//	dump dc.w ( words )
void dump_to_dbw(void *p,int count,FILE *fp,int w)
{
uint16_t *ptr =p;
uint32_t	total = count>>1;
	printf("deprecated\n");
	return;
	for (int c=0;c<total;c++)
	{
		int ci = c % w;
		if (ci==0)
			fprintf(fp,"dc.w ");
		fprintf(fp,"$%04X",*ptr++);							
		if (ci==w-1)
			fprintf(fp,"\n");
		else if (c!=(total-1))
			fprintf(fp,",");
	}
}

//	dump dc.b ( bytes )
void dump_to_dbb(void *p,int count,FILE *fp,int w)
{
uint8_t *ptr =p;
uint32_t	total = count;
	printf("deprecated\n");
	return;
	for (int c=0;c<total;c++)
	{
		int ci = c % w;
		if (ci==0)
			fprintf(fp,"dc.b ");
		fprintf(fp,"$%02X",*ptr++);							
		if (ci==w-1)
			fprintf(fp,"\n");
		else if (c!=(total-1))
			fprintf(fp,",");
	}
}

void dump_int(FILE *fo,uint16_t i)
{
	uint8_t b;
	b=(i>>8)&0xff;
	fwrite(&b,1,1,fo);
	b=i&0xff;
	fwrite(&b,1,1,fo);
}
void dump_nyb(FILE *fo,uint8_t a,uint8_t b)
{
	uint8_t o;
	o=((a&0xf)<<4) | (b&0xf);
	fwrite(&o,1,1,fo);
}

//	555 palette to 444 
void toasm_pal(const char *name)
{
uint32_t size;	
uint16_t *b = disk_load(name,&size);
	if (b!=NULL)
	{
		printf("writing %s_pal.asm",base_name);
		FILE *fo= newfile("_pal");
		RGB5551_PAL *gba=(RGB5551_PAL*)&b[0];
		for (int q=0;q<256;q++)
		{
			SEGA_PAL sega;
			sega.r = gba[q].r>>1;
			sega.g = gba[q].g>>1;
			sega.b = gba[q].b>>1;
			sega.a = 0;
			dump_int(fo,*(uint16_t*)&sega);
		}
		fclose(fo);
		free(b);
	}
}

//	convert GBA cel format
void toasm_cel(const char *name)
{
uint32_t size;	
uint8_t *b = disk_load(name,&size);
	if (b!=NULL)
	{
		printf("writing %s_cel.asm",base_name);
		FILE *fo= newfile("_cel");
		uint8_t*p = b;
		for (int y=0;y<size/8;y++)
		{
//			fprintf(fo,"  dc.b $%x%x,$%x%x,$%x%x,$%x%x\n",p[0]&0xf,p[1]&0xf,p[2]&0xf,p[3]&0xf,p[4]&0xf,p[5]&0xf,p[6]&0xf,p[7]&0xf);
			dump_nyb(fo,p[0],p[1]);
			dump_nyb(fo,p[2],p[3]);
			dump_nyb(fo,p[4],p[5]);
			dump_nyb(fo,p[6],p[7]);
			p=p+8;			
		}
		fclose(fo);
		free(b);
	}
}

//	convert a bmp to tiles
void toasm_bmp(const char *name)
{
uint32_t size;	
uint8_t *b = disk_load(name,&size);
uint8_t *pix = &b[size-(8)];
BMPHeader_t *bmp = (BMPHeader_t*)&b[0];

	if (bmp->width!=8)
	{
		printf("::ERROR:: BMP is expected to be 8*height\n");
		return;
	}

	if (b!=NULL)
	{
//		printf("writing %s_cel.asm",base_name);
		FILE *fo= newfile("_cel");
		for (int q=0;q<bmp->height/8;q++)
		{
			for (int y=0;y<8;y++)
			{
				uint8_t*p = pix;
				dump_nyb(fo,p[0],p[1]);
				dump_nyb(fo,p[2],p[3]);
				dump_nyb(fo,p[4],p[5]);
				dump_nyb(fo,p[6],p[7]);
				pix=pix-(8);
			}
		}
		fclose(fo);
		free(b);
	}
}

int				layerc = 0;
SEGA_TILE *sega_map[8];
uint16_t w,h;

void toasm_map(const char *name)
{
uint32_t size;	
uint16_t *b = disk_load(name,&size);
uint16_t *ptr = b;

	if (b!=NULL)
	{
		int count;
		w=b[0];
		h=b[2];
		ptr = b+4;
		printf("reading %s\n",name);
		count = w*h;

		COSMIGO_TILE *ctile=(COSMIGO_TILE*)&ptr[0];

		sega_map[layerc]=(SEGA_TILE*)malloc(sizeof(SEGA_TILE) * w * h);

		for (int q=0;q<count;q++)
		{
			SEGA_TILE *sega_tile=&sega_map[layerc][q];
			sega_tile->clut = 0;
			sega_tile->tile = ctile[q].tile;
			sega_tile->hflip = ctile[q].hflip;
			sega_tile->vflip = ctile[q].vflip;
			sega_tile->priority = false;
//			if (sega_tile->tile!=0)
		//		printf("%x %x %d\n",ctile[q].tile,sega_tile->tile,q);			
//			ptr[q] = *(uint16_t*)&sega_tile;
	//		dump_int(fo,*(uint16_t*)&sega_tile);
		}
		layerc++;
		free(b);
	}
}

void tolayers()
{
SEGA_TILE *base=sega_map[0];

	printf("layers %d\n",layerc);
	for (int q=1;q<layerc;q++)
	{
		SEGA_TILE *current=sega_map[q];
		printf("flatten %d\n",q);
		for (int z=0;z<w*h;z++)
		{
			SEGA_TILE t=	*(SEGA_TILE*)&current[z];
			if (base[z].tile!=0)
			{	
				t=	*(SEGA_TILE*)&base[z];
				t.priority = true;
			}
			*(SEGA_TILE*)&base[z]=t;
		}
	}
	FILE *fo = newfile("_map");
	for (int z=0;z<w*h;z++)
	{
		dump_int(fo,*(uint16_t*)&base[z]);
	}
	fclose(fo);		

	for (int q=0;q<layerc;q++)
	{
		fprintf(stderr,"free layer %d\n",q);

		if (sega_map[q]!=NULL)
			free(sega_map[q]);
	}

}

void toasm_txt(const char *name)
{
FILE *fp=fopen(name,"rt");
char path_buffer[_MAX_PATH];  
char drive[_MAX_DRIVE];  
char dir[_MAX_DIR];  
char fname[_MAX_FNAME];  
char ext[_MAX_EXT];  
	
	_splitpath( name, drive, dir, fname, ext );


	while(!feof(fp))
	{
		char namebuff[256];
		fscanf(fp,"%s\n",&namebuff[0]);
		sprintf(path_buffer,"%s%s",dir,namebuff);

		const char *ext = get_filename_ext(path_buffer);
		fprintf(stderr,"[%s] %s\n",namebuff,path_buffer);

		if (strncmp(ext,".map",4)==0)	toasm_map(namebuff);
		if (strncmp(ext,".cel",4)==0)	toasm_cel(path_buffer);
		if (strncmp(ext,".pal",4)==0)	toasm_pal(path_buffer);
		if (strncmp(ext,".bmp",4)==0)	toasm_bmp(path_buffer);

	}
}

int main(int argc,char *argv[])
{
int c;
	for (c=1;c<argc;c++)
	{
		const char *arg = get_argument(argv[c]);
		printf("arg ? %s\n",arg);
		if (strncmp(arg,"-o",2)==0) base_name = (char*)arg+2;
		if (strncmp(arg,"-p",2)==0) priority=true;
		const char *ext = get_filename_ext(argv[c]);
		if (strncmp(ext,".txt",4)==0)	toasm_txt(argv[c]);
		if (strncmp(ext,".map",4)==0)	toasm_map(argv[c]);
		if (strncmp(ext,".cel",4)==0)	toasm_cel(argv[c]);
		if (strncmp(ext,".pal",4)==0)	toasm_pal(argv[c]);
		if (strncmp(ext,".bmp",4)==0)	toasm_bmp(argv[c]);
	}
	tolayers();
	return 0;
}


